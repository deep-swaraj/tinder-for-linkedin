// Constants
import { ROUTES } from '@/constants/routes';

// Components
import HeroButton from './components/molecules/hero-button';

export default function Home() {
  return (
    <section className="h-full bg-[url('/hero-background-modified.jpg')] bg-cover bg-center">
      <div className="w-full flex items-center justify-center h-full opacity-90 bg-slate-900">
        <div className="px-2 md:px-0">
          <div className="w-full flex flex-col lg:flex-row lg:items-center items-start gap-4 md:gap-6 lg:gap-8 py-4 md:py-6 lg:py-8">
            <h1 className="text-3xl md:text-5xl lg:text-6xl xl:text-8xl font-light">
              Grow your Network
            </h1>
            <HeroButton redirectUrl={ROUTES.PEOPLE} label="Find people" />
          </div>
          <div className="w-full flex flex-col lg:flex-row lg:items-center items-start gap-4 md:gap-6 lg:gap-8 py-4 md:py-6 lg:py-8 mt-4 xl:mt-8">
            <h1 className="text-3xl md:text-5xl lg:text-6xl xl:text-8xl font-light">
              Post your Job
            </h1>
            <HeroButton redirectUrl={ROUTES.POST_JOB} label="Post Job" />
          </div>
        </div>
      </div>
    </section>
  );
}
