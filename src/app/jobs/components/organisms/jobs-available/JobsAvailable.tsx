'use client';
import { useRef, useState } from 'react';
import { useRouter } from 'next/navigation';

// Icons
import { Dot } from 'lucide-react';

// Models
import { Job } from '@/models/jobs';
import { User } from '@/models/user';
import { Applicant } from '@/models/applicants';

// Utils
import { cn } from '@/lib/utils';

// Constants
import { WORKPLACE_TYPE_TO_LABEL } from '@/constants/job-types';
import { ROUTES } from '@/constants/routes';

// Components
import Card, {
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from '@/components/atoms/card';
import Button from '@/components/atoms/button';
import { Toaster } from '@/components/atoms/toast';
import JobApplication from '../job-application';
import Chip from '@/components/molecules/chip';

interface JobsAvailableProps {
  jobs: Job[];
  user: User;
  applications: Applicant[];
}

function JobsAvailable(props: JobsAvailableProps) {
  const { jobs, user, applications } = props;
  const router = useRouter();
  const [currentJob, setCurrentJob] = useState<Job>();
  const jobApplications = useRef(
    jobs.reduce((prevJobs, currJob) => {
      const jobApplicantById = {
        [currJob.id]: {
          applicants: currJob.applicants,
        },
      };
      return { ...prevJobs, ...jobApplicantById };
    }, {} as { [x: string]: { applicants: number } })
  );

  return (
    <>
      <Toaster />
      <div className="flex items-center gap-4">
        <Chip
          label="View your jobs"
          onClick={() => router.push(ROUTES.MY_JOBS)}
          className="mb-4 cursor-pointer"
        />
        <Chip
          label="Post your job"
          onClick={() => router.push(ROUTES.POST_JOB)}
          className="mb-4 cursor-pointer"
        />
      </div>
      <div
        className={cn({
          'grid grid-cols-[repeat(auto-fill,_minmax(320px,_1fr))] auto-rows-auto gap-4':
            !currentJob,
        })}
      >
        {!currentJob &&
          jobs.map((job) => (
            <Card
              key={job.id}
              className="min-w-80 hover:bg-slate-800 flex flex-col"
            >
              <CardHeader>
                <CardTitle>{job.job_title}</CardTitle>
                <CardDescription>{job.company_name}</CardDescription>
              </CardHeader>
              <CardContent className="flex items-center mt-auto">
                <p>{WORKPLACE_TYPE_TO_LABEL[job.workplace_type]}</p>
                <Dot />
                <p className="text-sky-500">
                  {jobApplications?.current[job?.id]?.applicants || 0}{' '}
                  applicants
                </p>
              </CardContent>
              <CardFooter className="flex justify-end mt-auto">
                <Button
                  onClick={() =>
                    setCurrentJob({
                      ...job,
                      applicants:
                        jobApplications?.current[job?.id]?.applicants || 0,
                    })
                  }
                >
                  Details
                </Button>
              </CardFooter>
            </Card>
          ))}
        {currentJob && (
          <JobApplication
            job={currentJob}
            user={user}
            onBackClick={() => setCurrentJob(undefined)}
            applications={applications}
            onApply={(jobApplied) =>
              (jobApplications.current = {
                ...jobApplications.current,
                ...{
                  [jobApplied.id]: { applicants: jobApplied.applicants + 1 },
                },
              })
            }
          />
        )}
      </div>
    </>
  );
}

export default JobsAvailable;
