import { Dispatch, SetStateAction, useState } from 'react';
import { v4 as uuid_v4 } from 'uuid';

// Icons
import { ArrowLeftCircle, Dot } from 'lucide-react';

// Models
import { Job } from '@/models/jobs';
import { User } from '@/models/user';
import { Applicant } from '@/models/applicants';

// Hooks
import { useToast } from '@/components/atoms/toast';

// Utils
import { createClient } from '@/utils/supabase/client';

// Constants
import {
  EMPLOYMENT_TYPE_TO_LABEL,
  WORKPLACE_TYPE_TO_LABEL,
} from '@/constants/job-types';
import { EMPTY_ARRAY } from '@/constants/defaults';

// Components
import Button from '@/components/atoms/button';
import Chip from '@/components/molecules/chip';

interface JobApplicationProps {
  job: Job;
  onBackClick: () => void;
  user: User;
  applications: Applicant[];
  onApply: (job: Job) => void;
}

function applyToJob(
  job: Job,
  user: User,
  toast: any,
  setAppliedToJob: Dispatch<SetStateAction<boolean>>,
  onApply: (job: Job) => void
) {
  const supabase = createClient();
  const applicantsUpdateData = {
    id: uuid_v4(),
    created_at: new Date().toUTCString(),
    job_id: job.id,
    user_id: user.id,
  };
  const jobListingsUpdateData = {
    ...job,
    applicants: job.applicants + 1,
  };
  (async () => {
    const { error: applicantsError } = await supabase
      .from('applicants')
      .upsert(applicantsUpdateData);
    if (applicantsError) {
      toast({
        variant: 'destructive',
        title: 'Uh oh! Something went wrong.',
        description: 'Could not apply to this job',
      });
    }
    if (!applicantsError) {
      toast({
        description: 'Submitted your application',
      });
      onApply(job);
      setAppliedToJob(true);
      await supabase.from('job-listings').upsert(jobListingsUpdateData);
    }
  })();
}

function JobApplication(props: JobApplicationProps) {
  const { job, onBackClick, user, applications = EMPTY_ARRAY, onApply } = props;
  const [appliedToJob, setAppliedToJob] = useState(false);

  const { toast } = useToast();

  const alreadyApplied =
    !!applications.find((application) => application.job_id === job.id) ||
    appliedToJob ||
    !!job.applicants;

  return (
    <div>
      <Button
        variant="link"
        className="flex items-center gap-2 p-0 pr-4 py-4"
        onClick={onBackClick}
      >
        <ArrowLeftCircle /> Back
      </Button>
      <div className="mt-6">
        <div className="flex items-center justify-between">
          <h3 className="text-2xl font-semibold mb-0.5">{job.job_title}</h3>
          {alreadyApplied && (
            <p className="text-sky-400 text-sm font-semibold">
              Applied to this job
            </p>
          )}
        </div>
        <h5 className="text-xl font-light mb-0.5">{job.company_name}</h5>
        <div className="flex items-center mb-4">
          <p>{WORKPLACE_TYPE_TO_LABEL[job.workplace_type]}</p>
          <Dot />
          <p>{EMPLOYMENT_TYPE_TO_LABEL[job.employment_type]}</p>
          <Dot />
          <p>{appliedToJob ? job.applicants + 1 : job.applicants} applicants</p>
          <Dot />
          <p>Minimum experience {job.min_year_of_exp} years</p>
        </div>
        <div className="flex items-center gap-1 flex-wrap mb-4">
          {job.skills.map((skill) => (
            <Chip label={skill} key={skill} />
          ))}
        </div>
        <h6 className="text-lg font-semibold mb-0.5">Role description</h6>
        <pre className="whitespace-pre-wrap mb-4">{job.role_description}</pre>
        <h6 className="text-lg font-semibold mb-0.5">Role responsibilities</h6>
        <pre className="whitespace-pre-wrap">{job.responsibilities}</pre>
        <div className="flex justify-end mt-4">
          {!alreadyApplied && (
            <Button
              onClick={() =>
                applyToJob(job, user, toast, setAppliedToJob, onApply)
              }
            >
              Apply
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default JobApplication;
