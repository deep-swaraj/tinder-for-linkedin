'use client';

// Utils
import { createClient } from '@/utils/supabase/client';

// Models
import { Job } from '@/models/jobs';
import { User } from '@/models/user';
import { Applicant } from '@/models/applicants';

// Components
import JobsAvailable from './components/organisms/jobs-available';
import { useEffect, useState } from 'react';

function Page() {
  const [jobs, setJobs] = useState<any>([]);
  const [user, setUser] = useState<any>([]);
  const [applications, setApplications] = useState<any>([]);
  useEffect(() => {
    const supabase = createClient();
    const currentUserId = JSON.parse(localStorage?.getItem('user') || '{}').id;
    (async () => {
      const { data: _jobs } = await supabase
        .from('job-listings')
        .select()
        .eq('isActive', true);
      setJobs(_jobs);
      const { data: _user } = await supabase
        .from('user-table')
        .select('id, first_name, last_name, email_address, avatar')
        .eq('id', currentUserId);

      setUser(_user);
      const { data: _applications } = await supabase
        .from('applicants')
        .select('job_id, id, created_at');
      setApplications(_applications);
    })();
  }, []);

  return (
    <section className="p-4 lg:p-6 xl:p-8 h-full">
      <JobsAvailable
        jobs={jobs as Job[]}
        user={user?.[0] as User}
        applications={applications as Applicant[]}
      />
    </section>
  );
}

export default Page;
