import Login from './components/organisms/login';

function Page() {
  return (
    <section className="p-4 lg:p-6 xl:p-8 h-full flex justify-center items-center">
      <Login />
    </section>
  );
}

export default Page;
