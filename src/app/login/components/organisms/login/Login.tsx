'use client';

import { z } from 'zod';
import { useRouter } from 'next/navigation';
import { zodResolver } from '@hookform/resolvers/zod';
import { useForm } from 'react-hook-form';

// Utils
import getFormData from '@/utils/getFromData';

// Actions
import loginFormAction from '@/actions/login-form';

// Helpers
import formSchema from './helpers/login.formSchema';

// Constants
import { ROUTES } from '@/constants/routes';
import DEFAULT_VALUES from './constants/login.defaultValues';

// Components
import Button from '@/components/atoms/button';
import Form, { FormField } from '@/components/organisms/form';
import FormTextfield from '@/components/molecules/formTextfield';
import { Toaster, useToast } from '@/components/atoms/toast';

function Login() {
  const { toast } = useToast();
  const router = useRouter();

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: DEFAULT_VALUES,
  });

  function onSubmit(values: z.infer<typeof formSchema>) {
    loginFormAction(getFormData(values)).then((data) => {
      toast({
        variant: data.isError ? 'destructive' : 'default',
        description: data.message,
      });
      form.reset();
      if (!data.isError) {
        localStorage.setItem('user', JSON.stringify(data.data));
        router.push(ROUTES.JOBS);
      }
    });
  }

  return (
    <>
      <Toaster />
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(onSubmit)}
          className="max-w-screen-md w-full"
        >
          <FormField
            control={form.control}
            name="email_address"
            render={({ field }) => (
              <FormTextfield<z.infer<typeof formSchema>, 'email_address'>
                field={field}
                label="Email address"
                className="basis-1/2"
              />
            )}
          />
          <FormField
            control={form.control}
            name="password"
            render={({ field }) => (
              <FormTextfield<z.infer<typeof formSchema>, 'password'>
                field={field}
                label="Password"
                className="basis-1/2"
                type="password"
              />
            )}
          />

          <div className="flex items-center justify-between gap-2 mt-2">
            <Button
              type="button"
              variant="link"
              className="pl-0"
              onClick={() => router.push(ROUTES.JOIN_NOW)}
            >
              <h6 className="text-lg font-light text-sky-400">
                Not a member, Register yourself.
              </h6>
            </Button>
            <Button type="submit">Login</Button>
          </div>
        </form>
      </Form>
    </>
  );
}

export default Login;
