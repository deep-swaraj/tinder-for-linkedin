import { z } from 'zod';

const formSchema = z.object({
  email_address: z
    .string()
    .min(1, { message: 'Email is required' })
    .email('Enter a valid email'),
  password: z.string().min(8, 'Password should be at least 8 characters'),
});

export default formSchema;
