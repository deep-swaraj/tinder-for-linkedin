const DEFAULT_VALUES = {
  first_name: '',
  last_name: '',
  email_address: '',
  phone_number: '',
  password: '',
  confirm_password: '',
  resume: '',
};

export default DEFAULT_VALUES;
