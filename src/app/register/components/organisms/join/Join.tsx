'use client';

import { z } from 'zod';
import { useRouter } from 'next/navigation';
import { zodResolver } from '@hookform/resolvers/zod';
import { useForm } from 'react-hook-form';

// Utils
import getFormData from '@/utils/getFromData';

// Actions
import joinFormAction from '@/actions/join-form';

// Helpers
import formSchema from './helpers/join.formSchema';

// Constants
import DEFAULT_VALUES from './constants/join.formDefaults';
import { ROUTES } from '@/constants/routes';

// Components
import Button from '@/components/atoms/button';
import Form, { FormField } from '@/components/organisms/form';
import FormTextfield from '@/components/molecules/formTextfield';
import FormFileInput from '@/components/molecules/form-file-input';
import { Toaster, useToast } from '@/components/atoms/toast';

function Join() {
  const { toast } = useToast();
  const router = useRouter();
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: DEFAULT_VALUES,
  });
  const fileRef = form.register('resume', { required: true });

  function onSubmit(values: z.infer<typeof formSchema>) {
    joinFormAction(getFormData({ ...values, resume: values.resume[0] })).then(
      (data) => {
        toast({
          variant: data.isError ? 'destructive' : 'default',
          description: data.message,
        });
        form.reset();
        if (!data.isError) {
          router.push(ROUTES.LOGIN);
        }
      }
    );
  }

  return (
    <>
      <Toaster />
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(onSubmit)}
          className="max-w-screen-md w-full"
        >
          <div className="flex gap-2 mb-2">
            <FormField
              control={form.control}
              name="first_name"
              render={({ field }) => (
                <FormTextfield<z.infer<typeof formSchema>, 'first_name'>
                  field={field}
                  label="First Name"
                  className="basis-1/2"
                />
              )}
            />
            <FormField
              control={form.control}
              name="last_name"
              render={({ field }) => (
                <FormTextfield<z.infer<typeof formSchema>, 'last_name'>
                  field={field}
                  label="Last Name"
                  className="basis-1/2"
                />
              )}
            />
          </div>
          <div className="flex gap-2 mb-2">
            <FormField
              control={form.control}
              name="email_address"
              render={({ field }) => (
                <FormTextfield<z.infer<typeof formSchema>, 'email_address'>
                  field={field}
                  label="Email address"
                  className="basis-1/2"
                />
              )}
            />
            <FormField
              control={form.control}
              name="phone_number"
              render={({ field }) => (
                <FormTextfield<z.infer<typeof formSchema>, 'phone_number'>
                  field={field}
                  label="Mobile number"
                  className="basis-1/2"
                />
              )}
            />
          </div>
          <div className="flex gap-2">
            <FormField
              control={form.control}
              name="password"
              render={({ field }) => (
                <FormTextfield<z.infer<typeof formSchema>, 'password'>
                  field={field}
                  label="Password"
                  className="basis-1/2"
                  type="password"
                />
              )}
            />
            <FormField
              control={form.control}
              name="confirm_password"
              render={({ field }) => (
                <FormTextfield<z.infer<typeof formSchema>, 'confirm_password'>
                  field={field}
                  label="Confirm password"
                  className="basis-1/2"
                  type="password"
                />
              )}
            />
          </div>
          <FormField
            control={form.control}
            name="resume"
            render={() => (
              <FormFileInput<z.infer<typeof formSchema>, 'resume'>
                label="Upload your resume"
                placeholder="resume"
                accept="application/pdf"
                formRegister={fileRef}
              />
            )}
          />

          <div className="flex items-center justify-between gap-2 mt-2">
            <Button
              type="button"
              variant="link"
              className="pl-0"
              onClick={() => router.push(ROUTES.LOGIN)}
            >
              <h6 className="text-lg font-light text-sky-400">
                Already a member, Login instead.
              </h6>
            </Button>
            <Button type="submit">Join</Button>
          </div>
        </form>
      </Form>
    </>
  );
}

export default Join;
