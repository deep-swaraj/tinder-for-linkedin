import { z } from 'zod';

const MAX_FILE_SIZE = 3 * 1024 * 1024;

const formSchema = z
  .object({
    first_name: z.string().trim().min(3, 'Enter a valid first name'),
    last_name: z.string().trim().min(3, 'Enter a valid last name'),
    email_address: z
      .string()
      .min(1, { message: 'Email is required' })
      .email('Enter a valid email'),
    phone_number: z
      .string()
      .regex(
        /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
        'Mobile number not valid'
      ),
    password: z.string().min(8, 'Password should be at least 8 characters'),
    confirm_password: z.string().min(8, 'Passwords not matching'),
    resume:
      typeof window === 'undefined'
        ? z.any()
        : z
            .any()
            .refine((files) => files?.length == 1, 'Resume is required.')
            .refine(
              (files) => files?.[0]?.size <= MAX_FILE_SIZE,
              'Max file size is 3MB.'
            )
            .refine(
              (files) => ['application/pdf'].includes(files?.[0]?.type),
              'Please upload in pdf format only'
            ),
  })
  .refine(
    (values) => {
      return values.password === values.confirm_password;
    },
    {
      message: 'Passwords not matching',
      path: ['confirm_password'],
    }
  );

export default formSchema;
