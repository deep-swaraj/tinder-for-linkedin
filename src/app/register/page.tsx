// Components
import Join from './components/organisms/join';

function Page() {
  return (
    <section className="p-4 lg:p-6 xl:p-8 h-full flex justify-center items-center">
      <Join />
    </section>
  );
}

export default Page;
