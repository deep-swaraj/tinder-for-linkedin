'use client';

import { useRouter } from 'next/navigation';

// Components
import Button from '@/components/atoms/button';

interface HeroButtonProps {
  redirectUrl: string;
  label: string;
}

function HeroButton(props: HeroButtonProps) {
  const { redirectUrl, label } = props;
  const router = useRouter();
  return (
    <Button
      className="lg:x-12 xl:h-16 shadow-md"
      onClick={() => router.push(redirectUrl)}
    >
      <h6 className="lg: text-2xl xl:text-3xl font-normal">{label}</h6>
    </Button>
  );
}

export default HeroButton;
