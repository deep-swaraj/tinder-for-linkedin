// Type
import { Step } from '@/components/organisms/step';

// Components
import JobLeadForm from '../components/organisms/job-form/components/organisms/job-lead-form';
import JobDescriptionForm from '../components/organisms/job-form/components/organisms/job-description-form';

const STEPS: Step[] = [
  {
    id: '1',
    label: 'Job Pitch',
    Component: JobLeadForm,
    className: 'md:mb-4 hover:bg-slate-700',
    activeClassName: 'bg-slate-700',
  },
  {
    id: '2',
    label: 'Job Description',
    Component: JobDescriptionForm,
    className: 'md:mb-4 hover:bg-slate-700',
    activeClassName: 'bg-slate-700',
  },
];

export default STEPS;
