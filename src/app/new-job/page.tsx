// Components
import JobForm from './components/organisms/job-form';

async function Page() {
  return (
    <section className="p-4 lg:p-6 xl:p-8 h-full">
      <JobForm />
    </section>
  );
}

export default Page;
