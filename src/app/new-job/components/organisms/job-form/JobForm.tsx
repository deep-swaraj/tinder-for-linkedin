'use client';

import { Dispatch, SetStateAction, useRef, useState } from 'react';
import { v4 as uuid_v4 } from 'uuid';

// Hooks
import { useRouter } from 'next/navigation';

// Utils
import { createClient } from '@/utils/supabase/client';

// Hooks
import { Toaster, useToast } from '@/components/atoms/toast';

// Constants
import STEPS from '../../../constants/new-job.formSteps';

// Components
import Step from '@/components/organisms/step';
import AlertDialog, {
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
} from '@/components/atoms/alert-dialog';
import { ROUTES } from '@/constants/routes';

function insertData(
  data: any,
  onSummaryShow: Dispatch<SetStateAction<boolean>>,
  toast: any
) {
  const supabase = createClient();
  const currentUserId = JSON.parse(localStorage?.getItem('user') || '{}').id;
  const update = {
    id: uuid_v4(),
    created_at: new Date().toUTCString(),
    job_title: data.title,
    company_name: data.company,
    workplace_type: data.workplaceType,
    employment_type: data.employementType,
    role_description: data.role,
    responsibilities: data.responsibilities,
    skills: data.skills.map(({ skill }: { [x: string]: string }) => skill),
    updated_at: new Date().toUTCString(),
    user_id: currentUserId,
  };
  (async () => {
    const { error } = await supabase.from('job-listings').upsert(update);
    if (!error) {
      onSummaryShow(true);
    } else {
      toast({
        variant: 'destructive',
        title: 'Uh oh! Something went wrong.',
        description: 'Could not save post',
      });
    }
  })();
}

function JobForm() {
  const dataRef = useRef({ allData: {} });
  const [showSummary, setShowSummary] = useState(false);
  const [stepKey, setStepKey] = useState('');
  const router = useRouter();
  const { toast } = useToast();

  const nextDataHandler = (data: any, val: number, isComplete?: boolean) => {
    dataRef.current = {
      ...dataRef.current,
      [val]: {
        data,
      },
      allData: {
        ...dataRef.current?.allData,
        ...data,
      },
    };
    if (isComplete) {
      insertData(dataRef.current.allData, setShowSummary, toast);
    }
  };

  return (
    <>
      <Step
        steps={STEPS}
        className="h-full"
        onStepMove={nextDataHandler}
        defaultValuesRef={dataRef}
        key={stepKey}
      />
      <Toaster />
      <AlertDialog open={showSummary}>
        <AlertDialogContent>
          <AlertDialogHeader>
            <AlertDialogTitle>Saved your job post</AlertDialogTitle>
            <AlertDialogDescription>
              You can view your post or post another job
            </AlertDialogDescription>
          </AlertDialogHeader>
          <AlertDialogFooter>
            <AlertDialogCancel
              onClick={() => {
                setShowSummary(false);
                dataRef.current = { allData: {} };
                setStepKey(Math.random().toString());
              }}
            >
              Another Post
            </AlertDialogCancel>
            <AlertDialogAction
              onClick={() => {
                router.push(ROUTES.MY_JOBS);
              }}
            >
              View Post
            </AlertDialogAction>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog>
    </>
  );
}

export default JobForm;
