import { z } from 'zod';

// Constants
import { FIELD_IDS } from '../constants/job-description-form.formConfigs';

export const formSchema = z.object({
  [FIELD_IDS.SKILLS]: z
    .object({
      skill: z.string().trim(),
    })
    .array()
    .min(1, 'At-least one skill to be added'),
  [FIELD_IDS.ROLE]: z.string().min(1, 'Please describe about the role'),
  [FIELD_IDS.RESPONSIBILITIES]: z
    .string()
    .min(1, 'Please describe about the responsibilities'),
});
