import { z } from 'zod';

// Constants
import { FIELD_IDS } from '../constants/job-lead-form.formConfigs';

// Helpers
import {
  companyErrorValidator,
  employementTypeErrorValidator,
  titleErrorValidator,
  workplaceTypeErrorValidator,
} from './job-lead-form.errorValidators';

export const formSchema = z.object({
  [FIELD_IDS.TITLE]: z.string().trim().superRefine(titleErrorValidator),
  [FIELD_IDS.COMPANY]: z.string().trim().superRefine(companyErrorValidator),
  [FIELD_IDS.WORKPLACE_TYPE]: z
    .string()
    .superRefine(workplaceTypeErrorValidator),
  [FIELD_IDS.EMPLOYMENT_TYPE]: z
    .string()
    .superRefine(employementTypeErrorValidator),
});
