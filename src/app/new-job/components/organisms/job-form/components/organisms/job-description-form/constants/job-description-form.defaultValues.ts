// Constants
import { EMPTY_ARRAY } from '@/constants/defaults';
import { FIELD_IDS } from './job-description-form.formConfigs';

export const DEFAULT_VALUES = {
  [FIELD_IDS.ROLE]: '',
  [FIELD_IDS.RESPONSIBILITIES]: '',
  [FIELD_IDS.SKILLS]: EMPTY_ARRAY,
};
