'use client';

import { z } from 'zod';
import { zodResolver } from '@hookform/resolvers/zod';
import { useForm } from 'react-hook-form';

// Components
import Button from '@/components/atoms/button';
import FormTextfield from '@/components/molecules/formTextfield';
import FormSelect from '@/components/molecules/formSelect';
import Form, { FormField } from '@/components/organisms/form';

// Constants
import {
  DEFUALT_VALUES,
  WORKPLACE_OPTIONS,
  EMPLOYMENT_TYPE_OPTIONS,
} from './constants/job-lead-form.defaultValues';

// Schema
import { formSchema } from './helpers/job-lead-form.formSchema';

interface JobLeadFormProps {
  onNextClick?: (data: any) => void;
  onPrevClick?: (data: any) => void;
  defaultValue: any;
}

function JobLeadForm(props: JobLeadFormProps) {
  const { onNextClick, onPrevClick, defaultValue } = props;

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: defaultValue || DEFUALT_VALUES,
  });

  function onSubmit(values: z.infer<typeof formSchema>) {
    onNextClick && onNextClick(values);
    form.reset(DEFUALT_VALUES);
  }

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className="max-w-screen-md w-full"
      >
        <div className="block sm:flex sm:gap-2 sm:mb-2">
          <FormField
            control={form.control}
            name="title"
            render={({ field }) => (
              <FormTextfield<z.infer<typeof formSchema>, 'title'>
                field={field}
                label="Job Title"
                placeholder="Software Developer"
                className="basis-1/2"
              />
            )}
          />
          <FormField
            control={form.control}
            name="company"
            render={({ field }) => (
              <FormTextfield<z.infer<typeof formSchema>, 'company'>
                field={field}
                label="Company Name"
                placeholder="Tekion"
                className="basis-1/2"
              />
            )}
          />
        </div>
        <div className="block sm:flex sm:gap-2 sm:mb-2">
          <FormField
            control={form.control}
            name="workplaceType"
            render={({ field }) => (
              <FormSelect<z.infer<typeof formSchema>, 'workplaceType'>
                field={field}
                label="Workplace Type"
                placeholder="Select work environment"
                options={WORKPLACE_OPTIONS}
                className="basis-1/2"
              />
            )}
          />
          <FormField
            control={form.control}
            name="employementType"
            render={({ field }) => (
              <FormSelect<z.infer<typeof formSchema>, 'employementType'>
                field={field}
                label="Employment Type"
                placeholder="Select Employement Type"
                options={EMPLOYMENT_TYPE_OPTIONS}
                className="basis-1/2"
              />
            )}
          />
        </div>
        <div className="flex justify-end gap-2 mt-2">
          <Button
            type="button"
            variant="secondary"
            onClick={() => onPrevClick && onPrevClick(form.getValues())}
          >
            Previous
          </Button>
          <Button type="submit">Next</Button>
        </div>
      </form>
    </Form>
  );
}

export default JobLeadForm;
