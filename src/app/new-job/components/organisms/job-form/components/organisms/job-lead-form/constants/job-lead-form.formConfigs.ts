export const FIELD_IDS = {
  TITLE: 'title',
  COMPANY: 'company',
  WORKPLACE_TYPE: 'workplaceType',
  EMPLOYMENT_TYPE: 'employementType',
  DESCRIPTION: 'description',
  SKILLS: 'skills',
  SCREENING_QUESTIONS: 'screeningQuestions',
} as const;

export const FIELD_CONFIG = {
  [FIELD_IDS.TITLE]: {
    MIN_LENGTH: 5,
    MIN_LENGTH_ERROR: 'Job Title cannot be less than 5 characters.',
    MAX_LENGTH: 80,
    MAX_LENGTH_ERROR: 'Job Title cannot be more than 80 characters.',
    REQUIRED_ERROR: 'Job Title is required',
  },
  [FIELD_IDS.COMPANY]: {
    MIN_LENGTH: 3,
    MIN_LENGTH_ERROR: 'Company Name cannot be less than 3 characters.',
    MAX_LENGTH: 35,
    MAX_LENGTH_ERROR: 'Company Name cannot be more than 35 characters.',
    REQUIRED_ERROR: 'Company Name is required',
  },
  [FIELD_IDS.WORKPLACE_TYPE]: {
    REQUIRED_ERROR: 'Workplace Type is required',
  },
  [FIELD_IDS.EMPLOYMENT_TYPE]: {
    REQUIRED_ERROR: 'Employment Type is required',
  },
} as const;
