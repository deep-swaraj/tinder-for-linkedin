'use client';

import { z } from 'zod';
import { zodResolver } from '@hookform/resolvers/zod';
import { useFieldArray, useForm } from 'react-hook-form';

// Helpers
import { formSchema } from './helpers/job-description-form.formSchema';

// Constants
import { DEFAULT_VALUES } from './constants/job-description-form.defaultValues';

// Icons
import { X } from 'lucide-react';

// Components
import Button from '@/components/atoms/button';
import Form, { FormField } from '@/components/organisms/form';
import FormInputWithChips from '@/components/molecules/formInputWithChips';
import Chip from '@/components/molecules/chip';
import FormTextarea from '@/components/molecules/formTextarea';

interface JobDescriptionFormProps {
  onNextClick?: (data: any) => void;
  onPrevClick?: (data: any) => void;
  defaultValue?: any;
}

function JobDescriptionForm(props: JobDescriptionFormProps) {
  const { onNextClick, onPrevClick, defaultValue } = props;

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: defaultValue || DEFAULT_VALUES,
  });
  const { fields, append, remove } = useFieldArray({
    name: 'skills',
    control: form.control,
  });

  function onSubmit(values: z.infer<typeof formSchema>) {
    onNextClick && onNextClick(values);
    form.reset(DEFAULT_VALUES);
  }

  return (
    <Form {...form}>
      <form
        onSubmit={form.handleSubmit(onSubmit)}
        className="max-w-screen-md w-full"
      >
        <FormField
          control={form.control}
          name="role"
          render={({ field }) => (
            <FormTextarea<z.infer<typeof formSchema>, 'role'>
              field={field}
              label="Role Description"
              placeholder="Describe about the role"
            />
          )}
        />
        <FormField
          control={form.control}
          name="responsibilities"
          render={({ field }) => (
            <FormTextarea<z.infer<typeof formSchema>, 'responsibilities'>
              field={field}
              label="Responsibilities"
              placeholder="What are your expectations form the employee"
            />
          )}
        />
        <FormField
          control={form.control}
          name="skills"
          render={({ fieldState }) => {
            return (
              <FormInputWithChips
                minLengthMessage="Fill input to add skill"
                label="Add Skill"
                placeholder="React"
                buttonText="Add SKill"
                invalid={fieldState.invalid}
                errorMessage={fieldState.error?.message}
                onButtonClick={(data) => {
                  const isSkillIncluded = fields.find(
                    (value) => value.skill === data.inputValue
                  );
                  !isSkillIncluded && append({ skill: data.inputValue });
                }}
              >
                <div className="mt-2 mb-6 flex gap-2 flex-wrap h-24 overflow-auto">
                  {fields.map((field, index) => (
                    <Chip
                      label={field.skill}
                      key={field.id}
                      endIcon={
                        <X
                          onClick={() => remove(index)}
                          size={16}
                          className="cursor-pointer"
                        />
                      }
                    />
                  ))}
                </div>
              </FormInputWithChips>
            );
          }}
        />
        <div className="flex justify-end gap-2 mt-2">
          <Button
            type="button"
            variant="secondary"
            onClick={() => onPrevClick && onPrevClick(form.getValues())}
          >
            Previous
          </Button>
          <Button type="submit">Submit</Button>
        </div>
      </form>
    </Form>
  );
}

export default JobDescriptionForm;
