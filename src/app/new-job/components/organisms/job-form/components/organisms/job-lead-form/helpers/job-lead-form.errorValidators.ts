import { z } from 'zod';

// Constants
import {
  FIELD_IDS,
  FIELD_CONFIG,
} from '../constants/job-lead-form.formConfigs';

export function titleErrorValidator(val: string, ctx: z.RefinementCtx) {
  if (val.length == 0) {
    ctx.addIssue({
      code: z.ZodIssueCode.custom,
      message: FIELD_CONFIG[FIELD_IDS.TITLE].REQUIRED_ERROR,
    });
  }

  if (val.length < FIELD_CONFIG[FIELD_IDS.TITLE].MIN_LENGTH) {
    ctx.addIssue({
      code: z.ZodIssueCode.too_small,
      minimum: FIELD_CONFIG[FIELD_IDS.TITLE].MIN_LENGTH,
      type: 'string',
      inclusive: true,
      message: FIELD_CONFIG[FIELD_IDS.TITLE].MIN_LENGTH_ERROR,
    });
  }

  if (val.length > FIELD_CONFIG[FIELD_IDS.TITLE].MAX_LENGTH) {
    ctx.addIssue({
      code: z.ZodIssueCode.too_big,
      maximum: FIELD_CONFIG[FIELD_IDS.TITLE].MAX_LENGTH,
      type: 'string',
      inclusive: true,
      message: FIELD_CONFIG[FIELD_IDS.TITLE].MAX_LENGTH_ERROR,
    });
  }
}

export function companyErrorValidator(val: string, ctx: z.RefinementCtx) {
  if (val.length == 0) {
    ctx.addIssue({
      code: z.ZodIssueCode.custom,
      message: FIELD_CONFIG[FIELD_IDS.COMPANY].REQUIRED_ERROR,
    });
  }

  if (val.length < FIELD_CONFIG[FIELD_IDS.COMPANY].MIN_LENGTH) {
    ctx.addIssue({
      code: z.ZodIssueCode.too_small,
      minimum: FIELD_CONFIG[FIELD_IDS.COMPANY].MIN_LENGTH,
      type: 'string',
      inclusive: true,
      message: FIELD_CONFIG[FIELD_IDS.COMPANY].MIN_LENGTH_ERROR,
    });
  }

  if (val.length > FIELD_CONFIG[FIELD_IDS.COMPANY].MAX_LENGTH) {
    ctx.addIssue({
      code: z.ZodIssueCode.too_big,
      maximum: FIELD_CONFIG[FIELD_IDS.COMPANY].MAX_LENGTH,
      type: 'string',
      inclusive: true,
      message: FIELD_CONFIG[FIELD_IDS.COMPANY].MAX_LENGTH_ERROR,
    });
  }
}

export function workplaceTypeErrorValidator(val: string, ctx: z.RefinementCtx) {
  if (val.length == 0) {
    ctx.addIssue({
      code: z.ZodIssueCode.custom,
      message: FIELD_CONFIG[FIELD_IDS.WORKPLACE_TYPE].REQUIRED_ERROR,
    });
  }
}

export function employementTypeErrorValidator(
  val: string,
  ctx: z.RefinementCtx
) {
  if (val.length == 0) {
    ctx.addIssue({
      code: z.ZodIssueCode.custom,
      message: FIELD_CONFIG[FIELD_IDS.EMPLOYMENT_TYPE].REQUIRED_ERROR,
    });
  }
}
