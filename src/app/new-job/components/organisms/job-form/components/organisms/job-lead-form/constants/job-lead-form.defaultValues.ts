// Types
import { IOption } from '@/components/molecules/formSelect';

// Constants
import { FIELD_IDS } from '../constants/job-lead-form.formConfigs';
import { EMPLOYMENT_TYPES, WORKPLACE_TYPES } from '@/constants/job-types';

export const DEFUALT_VALUES = {
  [FIELD_IDS.TITLE]: '',
  [FIELD_IDS.COMPANY]: '',
  [FIELD_IDS.WORKPLACE_TYPE]: '',
  [FIELD_IDS.EMPLOYMENT_TYPE]: '',
};

export const WORKPLACE_OPTIONS: IOption[] = [
  { id: '1', value: WORKPLACE_TYPES.remote, label: 'Remote' },
  { id: '2', value: WORKPLACE_TYPES.hybrid, label: 'Hybrid' },
  { id: '3', value: WORKPLACE_TYPES.office, label: 'Office' },
];

export const EMPLOYMENT_TYPE_OPTIONS: IOption[] = [
  { id: '1', value: EMPLOYMENT_TYPES.internship, label: 'Internship' },
  { id: '2', value: EMPLOYMENT_TYPES['full-time'], label: 'Full Time' },
  { id: '3', value: EMPLOYMENT_TYPES.contract, label: 'Contract' },
];
