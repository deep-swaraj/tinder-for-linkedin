import type { Metadata } from 'next';
import { Inter } from 'next/font/google';

const inter = Inter({ subsets: ['latin'] });

//Components
import ThemeProvider from '@/components/atoms/theme-provider';
import Header from '@/components/organisms/header';
import Footer from '@/components/organisms/footer';

// Styles
import './globals.css';

export const metadata: Metadata = {
  title: 'Careersio',
  description:
    'A platform for your job hunts. Connect with peers and HRs just by swiping.',
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" dir="ltr" className="h-full" suppressHydrationWarning>
      <body className={`h-full flex flex-col ${inter.className}`}>
        <ThemeProvider attribute="class" defaultTheme="dark" enableSystem>
          <Header />
          <main className="flex-auto max-w-screen-2xl m-auto w-full">
            {children}
          </main>
          <Footer />
        </ThemeProvider>
      </body>
    </html>
  );
}
