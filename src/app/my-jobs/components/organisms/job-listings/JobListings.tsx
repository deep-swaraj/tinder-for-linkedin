'use client';

import { useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/navigation';

// Models
import { Job } from '@/models/jobs';

// Icons
import { ArrowLeftCircle, Dot } from 'lucide-react';

// Utils
import { cn } from '@/lib/utils';
import { createClient } from '@/utils/supabase/client';

// Constants
import { WORKPLACE_TYPE_TO_LABEL } from '@/constants/job-types';

// Components
import Card, {
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from '@/components/atoms/card';
import Button from '@/components/atoms/button';

interface JobListingsProps {
  jobs: Job[];
}

function JobListings(props: JobListingsProps) {
  const { jobs } = props;
  const router = useRouter();
  const [currentJobIdx, setCurrentJobIdx] = useState(0);
  const [currenJobApplicants, setCurrentJobApplicants] = useState<
    | {
        id: string;
        first_name: string;
        last_name: string;
        email_address: string;
        resume_path: string;
      }[]
    | null
    | undefined
  >([]);
  const currentJob = jobs[currentJobIdx];

  useEffect(() => {
    const supabase = createClient();
    (async () => {
      const { data: applications } = await supabase
        .from('applicants')
        .select('job_id, id, user_id, created_at')
        .eq('job_id', currentJob?.id);
      const userIds =
        applications?.map((application) => application.user_id) || [];
      const { data: userData } = await supabase
        .from('user-table')
        .select('id, first_name, last_name, email_address, resume_path')
        .in('id', userIds as readonly any[]);
      const updatedUserData = userData?.map((user) => {
        const { data: urlData } = supabase.storage
          .from('resumes')
          .getPublicUrl(
            user.resume_path || 'resume_sdafghjk_1708870058781.pdf'
          );
        return {
          ...user,
          resume_path: urlData.publicUrl,
        };
      });
      setCurrentJobApplicants(updatedUserData);
    })();
  }, [currentJob?.id]);

  return (
    <div className="w-full flex gap-6">
      <div className="min-w-80 w-1/4">
        <Button
          variant="link"
          className="flex items-center gap-2 pl-0 mb-4"
          onClick={() => router.back()}
        >
          <ArrowLeftCircle /> Back
        </Button>
        {jobs.map((job, index) => (
          <Card
            key={job.id}
            className={cn('cursor-pointer mb-4 last:mb-0 hover:bg-slate-800', {
              ['bg-slate-800']: currentJobIdx === index,
            })}
            onClick={() => setCurrentJobIdx(index)}
          >
            <CardHeader>
              <CardTitle>{job.job_title}</CardTitle>
              <CardDescription>{job.company_name}</CardDescription>
            </CardHeader>
            <CardContent className="flex items-center">
              <p>{WORKPLACE_TYPE_TO_LABEL[job.workplace_type]}</p>
              <Dot />
              <p
                className={cn({
                  'text-sky-500': job.isActive,
                  'text-rose-500': !job.isActive,
                })}
              >
                {job.isActive ? 'Active' : 'Closed'}
              </p>
              <Dot />
              <p className="text-sky-500">{job.applicants} applicants</p>
            </CardContent>
          </Card>
        ))}
      </div>
      <div className="mt-14 grid grid-cols-[repeat(auto-fill,_minmax(320px,_1fr))] auto-rows-auto gap-2 w-full">
        {currenJobApplicants?.map((applicant) => (
          <Card
            key={applicant.id}
            className={'cursor-pointer min-w-80 max-h-44 hover:bg-slate-800'}
          >
            <CardHeader>
              <CardTitle>
                {applicant.first_name} {applicant.last_name}
              </CardTitle>
            </CardHeader>
            <CardContent>{applicant.email_address}</CardContent>
            <CardFooter>
              <Button
                variant="link"
                className="pl-0"
                onClick={() => window.open(applicant.resume_path)}
              >
                View Resume
              </Button>
            </CardFooter>
          </Card>
        ))}
      </div>
    </div>
  );
}

export default JobListings;
