'use client';

// Utils
import { createClient } from '@/utils/supabase/client';

// Models
import { Job } from '@/models/jobs';

// Components
import JobListings from './components/organisms/job-listings';
import { useEffect, useState } from 'react';

function Page() {
  const [jobs, setJobs] = useState<any>([]);
  useEffect(() => {
    const supabase = createClient();
    const currentUserId = JSON.parse(localStorage?.getItem('user') || '{}').id;
    (async () => {
      const { data: _jobs } = await supabase
        .from('job-listings')
        .select()
        .eq('user_id', currentUserId);
      setJobs(_jobs);
    })();
  }, []);

  return (
    <section className="p-4 lg:p-6 xl:p-8 h-full">
      <JobListings jobs={jobs as Job[]} />
    </section>
  );
}

export default Page;
