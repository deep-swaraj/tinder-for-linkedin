'use server';

import bcrypt from 'bcrypt';
import { cookies } from 'next/headers';

// Utils
import { createClient } from '@/utils/supabase/server';

async function loginFormAction(values: FormData) {
  console.log(values);
  const cookieStore = cookies();
  const supabase = createClient(cookieStore);
  const email = values.get('email_address');
  const password = values.get('password') as string;

  const { data: userTableData, error: userTableError } = await supabase
    .from('user-table')
    .select('id, first_name, last_name, email_address, password')
    .eq('email_address', email)
    .single();

  if (userTableError) {
    if (userTableError.code === 'PGRST116') {
      return {
        isError: true,
        message: 'Email not registered',
      };
    }
    return {
      isError: true,
      message: 'Something went wrong, please try again later',
    };
  }
  const matched = await bcrypt.compare(password, userTableData?.password);
  if (!matched) {
    return {
      isError: true,
      message: 'Password is not correct',
    };
  }
  return {
    isError: false,
    message: 'Login successfull',
    data: {
      id: userTableData.id,
      firstName: userTableData.first_name,
      lastName: userTableData.last_name,
      email: userTableData.email_address,
    },
  };
}

export default loginFormAction;
