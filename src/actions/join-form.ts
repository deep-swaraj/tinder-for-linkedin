'use server';

import bcrypt from 'bcrypt';
import { cookies } from 'next/headers';
import { v4 as uuid_v4 } from 'uuid';

// Utils
import { createClient } from '@/utils/supabase/server';

async function joinFormAction(values: FormData) {
  const cookieStore = cookies();
  const supabase = createClient(cookieStore);

  const resume = values.get('resume') as File;
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(
    values.get('password') as string,
    salt
  );

  const { data, error: uploadError } = await supabase.storage
    .from('resumes')
    .upload(`resume_${values.get('first_name')}_${Date.now()}.pdf`, resume);
  if (uploadError) {
    return {
      isError: true,
      message: 'Error uploading resume, please try again in some time',
    };
  }
  const { path: resumePath } = data as any;

  const updateUser = {
    id: uuid_v4(),
    created_at: new Date().toUTCString(),
    first_name: values.get('first_name'),
    last_name: values.get('last_name'),
    email_address: values.get('email_address'),
    password: hashedPassword,
    resume_path: resumePath,
    updated_at: new Date().toUTCString(),
  };
  const { error: userTableError } = await supabase
    .from('user-table')
    .upsert(updateUser);

  if (userTableError) {
    if (userTableError.code === '23505') {
      return {
        isError: true,
        message: 'Email already exists',
      };
    }
    return {
      isError: true,
      message: 'Error creating user, please try again in some time',
    };
  }
  return {
    isError: false,
    message: 'Registration successfull',
  };
}

export default joinFormAction;
