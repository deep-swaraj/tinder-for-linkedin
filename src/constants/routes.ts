export const ROUTES = {
  HOME: '/',
  PEOPLE: '/people',
  JOBS: '/jobs',
  POST_JOB: '/new-job',
  JOIN_NOW: '/register',
  MY_JOBS: '/my-jobs',
  LOGIN: '/login',
} as const;
