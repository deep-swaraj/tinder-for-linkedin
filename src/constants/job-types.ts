export const WORKPLACE_TYPES = {
  remote: 'remote',
  hybrid: 'hybrid',
  office: 'office',
} as const;

export const EMPLOYMENT_TYPES = {
  internship: 'internship',
  'full-time': 'full-time',
  contract: 'contract',
} as const;

export const WORKPLACE_TYPE_TO_LABEL = {
  [WORKPLACE_TYPES.hybrid]: 'Hybrid',
  [WORKPLACE_TYPES.office]: 'Office',
  [WORKPLACE_TYPES.remote]: 'Remote',
};

export const EMPLOYMENT_TYPE_TO_LABEL = {
  [EMPLOYMENT_TYPES.contract]: 'Contract',
  [EMPLOYMENT_TYPES['full-time']]: 'Full time',
  [EMPLOYMENT_TYPES.internship]: 'Internship',
};
