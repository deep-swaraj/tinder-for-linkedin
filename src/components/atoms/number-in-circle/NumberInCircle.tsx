export interface NumberInCircleProps {
  number: string | number;
}

function NumberInCircle(props: NumberInCircleProps) {
  const { number } = props;
  return (
    <div className="h-8 w-8 rounded-full bg-slate-800 flex items-center justify-center">
      <h6 className="font-bold">{number}</h6>
    </div>
  );
}

export default NumberInCircle;
