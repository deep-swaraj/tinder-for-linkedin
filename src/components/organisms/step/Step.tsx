'use client';

import { ComponentType, MutableRefObject, ReactNode, useState } from 'react';

// Utils
import { cn } from '@/lib/utils';

// Icons
import { ChevronDown } from 'lucide-react';

// Components
import StepLabel from '@/components/molecules/step-label';
import DropdownMenu, {
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from '@/components/molecules/dropdown-menu';

export interface Step {
  id: string;
  label: string;
  className?: string;
  labelClassName?: string;
  activeClassName?: string;
  Component: ComponentType<any>;
}

export interface StepProps {
  steps: Step[];
  defaultStep?: number;
  className?: string;
  onStepMove?: (values: any, stepNumber: number, isComplete?: boolean) => void;
  defaultValuesRef?: MutableRefObject<any>;
}

function stepRenderer(currentStepIdx: number) {
  return function renderStep(step: Step, index: number) {
    const { id, label, className, labelClassName, activeClassName = '' } = step;
    const isActiveStep = index == currentStepIdx;

    return (
      <StepLabel
        number={index + 1}
        label={label}
        className={cn(className, {
          [activeClassName]: isActiveStep,
        })}
        labelClassName={labelClassName}
        key={id}
        id={index}
      />
    );
  };
}

function renderMobileSteps(currentStepIdx: number, steps: Step[]) {
  const currentStep = steps[currentStepIdx];
  const {
    id,
    label,
    className,
    labelClassName,
    activeClassName = '',
  } = currentStep;
  return (
    <DropdownMenu>
      <DropdownMenuTrigger className="flex gap-4 justify-start items-center w-full">
        <StepLabel
          key={id}
          number={currentStepIdx + 1}
          label={label}
          className={cn(className, activeClassName, 'w-full')}
          labelClassName={labelClassName}
          id={currentStepIdx}
        />
        <ChevronDown className="m-4" />
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        {steps.map((step, index) => {
          const { id, label, className, labelClassName } = step;
          return (
            <DropdownMenuItem className={className} key={id}>
              <h6 className={labelClassName}>{label}</h6>
            </DropdownMenuItem>
          );
        })}
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

function Step(props: StepProps) {
  const {
    defaultStep = 0,
    steps,
    className,
    onStepMove,
    defaultValuesRef,
  } = props;
  const [currentStepIdx, setCurrentStepIdx] = useState(defaultStep);

  if (defaultStep >= steps.length) {
    throw new Error('defaultStep greater than steps.length');
  }

  const currentStep = steps[currentStepIdx];
  const handleStepClick = (clickedId: number) => setCurrentStepIdx(clickedId);

  return (
    <div
      className={cn(
        'flex flex-col gap-8 md:flex-row md:gap-4 lg:gap-8 xl:gap-12 w-full justify-start md:items-center md:justify-center',
        className
      )}
    >
      <aside className="sm:hidden">
        {renderMobileSteps(currentStepIdx, steps)}
      </aside>
      <aside className="hidden sm:flex gap-4 md:block md:min-w-56">
        {steps.map(stepRenderer(currentStepIdx))}
      </aside>

      <currentStep.Component
        onNextClick={(values: any) => {
          onStepMove &&
            onStepMove(
              values,
              currentStepIdx,
              currentStepIdx === steps.length - 1
            );
          setCurrentStepIdx((index) => {
            return index >= steps.length - 1 ? 0 : index + 1;
          });
        }}
        onPrevClick={(values: any) => {
          onStepMove && onStepMove(values, currentStepIdx);
          setCurrentStepIdx((index) => (index === 0 ? 0 : index - 1));
        }}
        defaultValue={defaultValuesRef?.current[currentStepIdx]?.data}
      />
    </div>
  );
}

export default Step;
