'use client';

function getLoggedInUser() {
  const user = JSON.parse(localStorage?.getItem('user') || '{}');
  return user;
}

export default getLoggedInUser;
