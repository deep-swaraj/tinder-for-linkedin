'use client';

// Helpers
import getLoggedInUser from './helpers/header.userLogin';

// Components
import Logo from './components/atoms/logo';
import UserActions from './components/organisms/user-actions';

function Header() {
  const user = getLoggedInUser();
  return (
    <header className="h-16 md:h-24 lg:h-36 bg-inherit shrink-0">
      <nav className="w-full p-2 md:p-4 lg:p-6 fixed top-0 z-50 flex justify-center bg-inherit">
        <div className="h-12 md:h-16 lg:h-24 max-w-screen-2xl w-full flex items-center justify-between">
          <Logo />
          <UserActions user={user} />
        </div>
      </nav>
    </header>
  );
}

export default Header;
