import ActionItems from '../../molecules/actionItems';
import DropdownActionitems from '../../molecules/dropdown-actionitems';

interface UserActionsProps {
  user: {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
  };
}

function UserActions(props: UserActionsProps) {
  const { user } = props;
  return (
    <>
      <ActionItems firstName={user?.firstName} />
      <DropdownActionitems />
    </>
  );
}

export default UserActions;
