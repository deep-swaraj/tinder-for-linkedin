'use client';

import { useRouter } from 'next/navigation';

// Types
import { LucideIcon } from 'lucide-react';

// Components
import Button from '@/components/atoms/button';

interface ActionItemProps {
  Icon?: LucideIcon;
  variant:
    | 'link'
    | 'default'
    | 'destructive'
    | 'outline'
    | 'secondary'
    | 'ghost'
    | undefined;
  label: string;
  redirectUrl: string;
}

function ActionItem(props: ActionItemProps) {
  const { Icon, variant, label, redirectUrl } = props;
  const router = useRouter();
  return (
    <Button
      variant={variant}
      className="flex items-center gap-1"
      onClick={() => router.push(redirectUrl)}
    >
      {Icon && <Icon />}
      <h6 className="font-semibold">{label}</h6>
    </Button>
  );
}

export default ActionItem;
