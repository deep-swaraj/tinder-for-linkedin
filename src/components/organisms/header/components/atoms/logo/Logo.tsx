'use client';

import { useRouter } from 'next/navigation';

// Constants
import { ROUTES } from '@/constants/routes';

// Components
import Image from 'next/image';

function Logo() {
  const router = useRouter();
  return (
    <Image
      src="/DarkModeLogo.svg"
      alt="CareerSio Logo"
      className="w-auto h-12 md:h-16 lg:h-24 cursor-pointer"
      width={0}
      height={0}
      priority
      draggable={false}
      onClick={() => router.push(ROUTES.HOME)}
    />
  );
}

export default Logo;
