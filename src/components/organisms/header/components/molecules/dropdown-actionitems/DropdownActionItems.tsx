'use client';

import { useRouter } from 'next/navigation';

// Icons
import { Menu, LampDesk, Users } from 'lucide-react';

// Constants
import { ROUTES } from '@/constants/routes';

// Components
import DropdownMenu, {
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from '@/components/molecules/dropdown-menu';

function DropdownActionItems() {
  const router = useRouter();
  return (
    <div className="flex md:hidden items-center gap-3">
      <DropdownMenu>
        <DropdownMenuTrigger>
          <Menu />
        </DropdownMenuTrigger>
        <DropdownMenuContent align="end">
          <DropdownMenuItem
            className="flex items-center gap-1.5"
            onClick={() => router.push(ROUTES.JOBS)}
          >
            <LampDesk />
            <h6 className="font-semibold">Jobs</h6>
          </DropdownMenuItem>
          <DropdownMenuItem
            className="flex items-center gap-1.5"
            onClick={() => router.push(ROUTES.PEOPLE)}
          >
            <Users />
            <h6 className="font-semibold">People</h6>
          </DropdownMenuItem>
          <DropdownMenuSeparator className="bg-gray-400" />
          <DropdownMenuItem onClick={() => router.push(ROUTES.JOIN_NOW)}>
            <h6 className="font-semibold">Join Now</h6>
          </DropdownMenuItem>
        </DropdownMenuContent>
      </DropdownMenu>
    </div>
  );
}
export default DropdownActionItems;
