'use client';

// Icons
import { LampDesk, Users } from 'lucide-react';

// Constants
import { ROUTES } from '@/constants/routes';

// Components
import ActionItem from '../../atoms/actionItem';

function ActionItems(props: { firstName: string }) {
  const { firstName } = props;
  return (
    <>
      <div className="hidden md:flex items-center gap-3">
        <div className="flex items-center gap-2 pr-3 border-r border-gray-400">
          <ActionItem
            label="Jobs"
            variant="ghost"
            Icon={LampDesk}
            redirectUrl={ROUTES.JOBS}
          />
          <ActionItem
            label="People"
            variant="ghost"
            Icon={Users}
            redirectUrl={ROUTES.PEOPLE}
          />
        </div>
        {firstName ? (
          <h6 className="text-sm font-semibold">Welcome {firstName}</h6>
        ) : (
          <ActionItem
            label="Join Now"
            variant="default"
            redirectUrl={ROUTES.JOIN_NOW}
          />
        )}
      </div>
    </>
  );
}

export default ActionItems;
