// Icons
import { Heart } from 'lucide-react';

function Footer() {
  return (
    <footer className="py-4 flex justify-center">
      <div className="max-w-screen-2xl w-full flex items-center justify-center">
        <h6 className="font-semibold flex items-center gap-1">
          Made with <Heart className="h-4 w-4" /> for hackathon
        </h6>
      </div>
    </footer>
  );
}

export default Footer;
