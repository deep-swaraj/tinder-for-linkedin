// Utils
import { cn } from '@/lib/utils';

// Components
import {
  FormControl,
  FormDescription,
  FormItem,
  FormLabel,
  FormMessage,
  ControllerRenderProps,
  FieldValues,
  FieldPath,
} from '@/components/organisms/form';
import Input from '@/components/atoms/input';

interface FormTextfieldProps<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
> {
  label?: string;
  field: ControllerRenderProps<TFieldValues, TName>;
  placeholder?: string;
  description?: string;
  className?: string;
  invalid?: boolean;
  errorMessage?: string;
  type?: string;
}

function FormTextfield<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
>(props: FormTextfieldProps<TFieldValues, TName>) {
  const {
    label,
    field,
    placeholder,
    description,
    errorMessage,
    invalid,
    className,
    type,
  } = props;
  return (
    <FormItem className={className}>
      <FormLabel className={cn({ 'text-destructive': invalid })}>
        {label}
      </FormLabel>
      <FormControl>
        <Input placeholder={placeholder} {...field} type={type} />
      </FormControl>
      {description && <FormDescription>{description}</FormDescription>}
      <FormMessage>{errorMessage}</FormMessage>
    </FormItem>
  );
}

export default FormTextfield;
