'use client';

import { ReactNode, useRef } from 'react';

// Utils
import { cn } from '@/lib/utils';

// Hooks
import useTooltipVisibility from './hooks/useTooltipVisibility';

// Components
import Tooltip, {
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from '@/components/atoms/tooltip';

interface ChipProps {
  label?: string;
  endIcon?: ReactNode;
  className?: string;
  onClick?: () => void;
}

function renderWithTooltip(label: string) {
  return (
    <TooltipProvider>
      <Tooltip>
        <TooltipTrigger>
          <div className="text-ellipsis whitespace-nowrap max-w-56 overflow-hidden">
            {label}
          </div>
        </TooltipTrigger>
        <TooltipContent className="mb-2">
          <p className="max-w-56 break-words">{label}</p>
        </TooltipContent>
      </Tooltip>
    </TooltipProvider>
  );
}

function Chip(props: ChipProps) {
  const { label = '', endIcon, className, onClick } = props;

  const tooltipRef = useRef<HTMLDivElement>(null);
  const showTooltip = useTooltipVisibility(tooltipRef);

  return (
    <div
      className={cn(
        'flex items-center gap-2 rounded-full h-fit w-fit py-2 px-4 border-slate-700 border-2',
        className
      )}
      onClick={onClick}
    >
      {showTooltip ? (
        renderWithTooltip(label)
      ) : (
        <div
          className="text-ellipsis whitespace-nowrap max-w-56 overflow-hidden"
          ref={tooltipRef}
        >
          {label}
        </div>
      )}
      {endIcon}
    </div>
  );
}

export default Chip;
