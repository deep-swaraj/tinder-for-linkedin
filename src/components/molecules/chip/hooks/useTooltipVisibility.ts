import { MutableRefObject, RefObject, useEffect, useState } from 'react';

function useTooltipVisibility<T extends HTMLElement | null>(
  tooltipRef: MutableRefObject<T>
) {
  const [showTooltip, setShowTooltip] = useState(false);

  useEffect(() => {
    const container = tooltipRef.current;
    if (container) {
      setShowTooltip(container.scrollWidth > container.clientWidth);
    }
  }, [tooltipRef]);

  return showTooltip;
}

export default useTooltipVisibility;
