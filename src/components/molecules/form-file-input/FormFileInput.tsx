import * as React from 'react';

// Utils
import { cn } from '@/lib/utils';

// Components
import {
  FormControl,
  FormDescription,
  FormItem,
  FormLabel,
  FormMessage,
  ControllerRenderProps,
  FieldValues,
  FieldPath,
} from '@/components/organisms/form';
import FileInput from '../file-input';
import { UseFormRegisterReturn } from 'react-hook-form';

interface FormFileInputProps<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
> {
  label?: string;
  field?: ControllerRenderProps<TFieldValues, TName>;
  placeholder?: string;
  description?: string;
  className?: string;
  invalid?: boolean;
  errorMessage?: string;
  accept?: string;
  formRegister?: UseFormRegisterReturn<TName>;
}

function FormFileInput<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
>(props: FormFileInputProps<TFieldValues, TName>) {
  const {
    label,
    field,
    placeholder,
    description,
    errorMessage,
    invalid,
    className,
    accept,
    formRegister,
  } = props;
  return (
    <FormItem className={className}>
      <FormLabel className={cn({ 'text-destructive': invalid })}>
        {label}
      </FormLabel>
      <FormControl>
        <FileInput
          placeholder={placeholder}
          {...field}
          {...formRegister}
          accept={accept}
        />
      </FormControl>
      {description && <FormDescription>{description}</FormDescription>}
      <FormMessage>{errorMessage}</FormMessage>
    </FormItem>
  );
}

export default FormFileInput;
