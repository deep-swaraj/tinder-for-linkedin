// Utils
import { cn } from '@/lib/utils';

// Components
import NumberInCircle, {
  NumberInCircleProps,
} from '@/components/atoms/number-in-circle';

interface StepLabelProps extends NumberInCircleProps {
  label: string;
  className?: string;
  labelClassName?: string;
  onClick?: (id: number) => void;
  id: number;
}

function StepLabel(props: StepLabelProps) {
  const {
    label,
    number,
    className,
    labelClassName,
    onClick = () => {},
    id,
  } = props;
  return (
    <div
      className={cn(
        'flex items-center gap-2 rounded-md py-2 pl-2 pr-4 flex-auto',
        className
      )}
      onClick={() => onClick(id)}
    >
      <NumberInCircle number={number} />
      <h6 className={cn('font-extralight', labelClassName)}>{label}</h6>
    </div>
  );
}

export default StepLabel;
