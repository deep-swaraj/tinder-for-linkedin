import { cn } from '@/lib/utils';

// Components
import {
  FormControl,
  FormDescription,
  FormItem,
  FormLabel,
  FormMessage,
  ControllerRenderProps,
  FieldValues,
  FieldPath,
} from '@/components/organisms/form';
import Textarea from '@/components/atoms/textarea';

interface FormTextareaProps<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
> {
  label?: string;
  field: ControllerRenderProps<TFieldValues, TName>;
  placeholder?: string;
  description?: string;
  fieldClassName?: string;
}

function FormTextarea<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
>(props: FormTextareaProps<TFieldValues, TName>) {
  const { label, field, placeholder, description, fieldClassName } = props;
  return (
    <FormItem>
      <FormLabel>{label}</FormLabel>
      <FormControl>
        <Textarea
          placeholder={placeholder}
          {...field}
          className={cn('resize-none', fieldClassName)}
        />
      </FormControl>
      <FormDescription>{description}</FormDescription>
      <FormMessage />
    </FormItem>
  );
}

export default FormTextarea;
