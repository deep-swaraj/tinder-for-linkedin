import * as React from 'react';

// Utils
import { cn } from '@/lib/utils';

// Components
import Input from '@/components/atoms/input';
import Label from '@/components/atoms/label';

export interface FileInputProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  label?: string;
  id?: string;
  className?: string;
}

const FileInput = React.forwardRef<HTMLInputElement, FileInputProps>(
  (props: FileInputProps, ref) => {
    const { label, id = 'fileInput', className, ...rest } = props;

    return (
      <div className={cn('grid w-full items-center gap-1', className)}>
        <Label htmlFor={id}>{label}</Label>
        <Input
          id={id}
          type="file"
          ref={ref}
          className="cursor-pointer"
          name="resume"
          {...rest}
        />
      </div>
    );
  }
);

FileInput.displayName = 'File Input';

export default FileInput;
