// Components
import {
  FormControl,
  FormDescription,
  FormItem,
  FormLabel,
  FormMessage,
  ControllerRenderProps,
  FieldValues,
  FieldPath,
} from '@/components/organisms/form';
import Select, {
  SelectTrigger,
  SelectValue,
  SelectContent,
  SelectItem,
} from '@/components/atoms/select';

export interface IOption {
  value: string;
  label: string;
  id: string;
}

export interface FormSelectProps<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
> {
  label?: string;
  field: ControllerRenderProps<TFieldValues, TName>;
  placeholder?: string;
  description?: string;
  options?: IOption[];
  className?: string;
}

function FormSelect<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
>(props: FormSelectProps<TFieldValues, TName>) {
  const { field, label, placeholder, description, options, className } = props;

  return (
    <FormItem className={className}>
      <FormLabel>{label}</FormLabel>
      <Select onValueChange={field.onChange} defaultValue={field.value}>
        <FormControl>
          <SelectTrigger>
            <SelectValue placeholder={placeholder} />
          </SelectTrigger>
        </FormControl>
        <SelectContent>
          {options?.map((option) => (
            <SelectItem key={option.id} value={option.value}>
              {option.label}
            </SelectItem>
          ))}
        </SelectContent>
      </Select>
      <FormDescription>{description}</FormDescription>
      <FormMessage />
    </FormItem>
  );
}

export default FormSelect;
