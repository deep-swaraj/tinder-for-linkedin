export { default } from './FormSelect';

export type { FormSelectProps, IOption } from './FormSelect';
