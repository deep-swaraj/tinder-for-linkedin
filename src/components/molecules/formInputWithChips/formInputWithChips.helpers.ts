import { z } from 'zod';

// Constants
import { FORM_IDS } from './formInputWithChips.constants';

interface FormSchemaProps {
  minLength?: number;
  minLengthMessage?: string;
}

export const getFormSchema = ({
  minLength = 1,
  minLengthMessage = 'This Field is Required',
}: FormSchemaProps) => {
  return z.object({
    [FORM_IDS.INPUT_VALUE]: z
      .string()
      .min(minLength, { message: minLengthMessage }),
  });
};
