export const FORM_IDS = {
  INPUT_VALUE: 'inputValue',
} as const;

export const DEFAULT_VALUES = {
  [FORM_IDS.INPUT_VALUE]: '',
};
