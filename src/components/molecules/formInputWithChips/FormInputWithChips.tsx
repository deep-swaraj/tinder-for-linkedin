import { cn } from '@/lib/utils';
import { z } from 'zod';
import { zodResolver } from '@hookform/resolvers/zod';
import { ReactElement } from 'react';
import { useForm } from 'react-hook-form';

// Constants
import { DEFAULT_VALUES, FORM_IDS } from './formInputWithChips.constants';

// Helpers
import { getFormSchema } from './formInputWithChips.helpers';

// Components
import FormTextfield from '@/components/molecules/formTextfield';
import { FormField } from '@/components/organisms/form';
import Button from '@/components/atoms/button';

interface FormInputWithChipsProps {
  children: ReactElement;
  minLengthMessage: string;
  label?: string;
  placeholder?: string;
  buttonText?: string;
  onButtonClick?: (data: { [FORM_IDS.INPUT_VALUE]: string }) => void;
  className?: string;
  invalid?: boolean;
  errorMessage?: string;
}

function FormInputWithChips(props: FormInputWithChipsProps) {
  const {
    children,
    minLengthMessage,
    label,
    placeholder,
    buttonText,
    className,
    invalid,
    errorMessage,
    onButtonClick,
  } = props;

  const formSchema = getFormSchema({ minLengthMessage });

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: DEFAULT_VALUES,
  });

  return (
    <>
      <div className={cn('flex items-center gap-2', className)}>
        <FormField
          control={form.control}
          name="inputValue"
          render={({ fieldState, field }) => (
            <FormTextfield<z.infer<typeof formSchema>, 'inputValue'>
              field={field}
              label={label}
              placeholder={placeholder}
              invalid={invalid || fieldState.invalid}
              errorMessage={errorMessage || fieldState.error?.message}
            />
          )}
        />
        <Button
          onClick={form.handleSubmit((data) => {
            if (onButtonClick) onButtonClick(data);
            form.resetField('inputValue');
            form.setFocus('inputValue');
          })}
          size={'sm'}
        >
          {buttonText}
        </Button>
      </div>
      {children}
    </>
  );
}

export default FormInputWithChips;
