// Constants
import { EMPLOYMENT_TYPES, WORKPLACE_TYPES } from '@/constants/job-types';

export interface Job {
  id: string;
  created_at: Date;
  job_title: string;
  company_name: string;
  workplace_type: keyof typeof WORKPLACE_TYPES;
  employment_type: keyof typeof EMPLOYMENT_TYPES;
  role_description: string;
  responsibilities: string;
  skills: string[];
  updated_at: Date;
  user_id: string;
  isActive: boolean;
  min_year_of_exp: number;
  applicants: number;
}
