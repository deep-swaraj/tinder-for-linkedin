export interface Applicant {
  id: string;
  created_at: Date;
  user_id: string;
  job_id: string;
}
